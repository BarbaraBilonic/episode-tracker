package barbarabilonic.ferit.episodetracker;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class CurrentlyWatching extends Fragment {
    private List<Shows> shows;
    private RecyclerAdapterCurrentlyWatching recyclerAdapter;
    private RecyclerView recyclerView;
   private  ButtonClickListener showCompletedBtn;
   public static final String SHARED_PREF_FILE="shared_pref_cv";
   public static final String KEY="currentlyWatching";







    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

           this.showCompletedBtn = (ButtonClickListener) context;




    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(SHARED_PREF_FILE, Context.MODE_PRIVATE);
        Gson gson=new Gson();
        String json=sharedPreferences.getString(KEY,null);
        Type type=new TypeToken<ArrayList<Shows>>() {}.getType();
        shows=gson.fromJson(json,type);
        if(shows==null){
            shows=new ArrayList<>();
        }




    }


    @Override
    public void onDetach() {
        super.onDetach();




}




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view= inflater.inflate(R.layout.currently_watching_fragment, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.CurrentlyWatchingRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerAdapter = new RecyclerAdapterCurrentlyWatching(showCompletedBtn);

        recyclerView.setAdapter(recyclerAdapter);
        if(shows.size()>0){
            recyclerAdapter.addData(shows);
        }








    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        showCompletedBtn = null;


    }



    public void addItem(Shows show){
        Shows sh=new Shows();
        sh.setImageLink(show.getImageLink());
        sh.setTitle(show.getTitle());
        sh.setEpisodeNumber(1);

        shows.add(sh);
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(SHARED_PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson=new Gson();
        String json=gson.toJson(shows);
        editor.clear();
        editor.putString(KEY,json);
        editor.apply();



        recyclerAdapter.addItem(sh);


    }

   public void removeItem(Shows show){
        for(int i=0;i<shows.size();i++){
            if(shows.get(i).getImageLink()==show.getImageLink()){
                shows.remove(i);
            }
        }
       SharedPreferences sharedPreferences = getContext().getSharedPreferences(SHARED_PREF_FILE, Context.MODE_PRIVATE);
       SharedPreferences.Editor editor = sharedPreferences.edit();
       Gson gson=new Gson();
       String json=gson.toJson(shows);
       editor.clear();
       editor.putString(KEY,json);
       editor.apply();



        recyclerAdapter.removeItem(show);

    }

    public void upEpisodeNumber(Shows show){
        for(int i=0;i<shows.size();i++){
            if(shows.get(i).getImageLink()==show.getImageLink()){
                shows.get(i).UpEpisodeNumber();
            }
        }
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(SHARED_PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson=new Gson();
        String json=gson.toJson(shows);
        editor.clear();
        editor.putString(KEY,json);
        editor.apply();
    }




}
