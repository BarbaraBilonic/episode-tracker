package barbarabilonic.ferit.episodetracker;

public interface ButtonClickListener {
    void CompleteButtonClicked(Shows show);
    void ChooseShowButtonClicked(Shows show);
    void upEpNumber(Shows show);
}
