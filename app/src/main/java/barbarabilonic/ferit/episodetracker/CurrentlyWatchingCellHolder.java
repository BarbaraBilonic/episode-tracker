package barbarabilonic.ferit.episodetracker;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

public class CurrentlyWatchingCellHolder extends RecyclerView.ViewHolder {
    ImageView ivCurrentlyWatching;
    TextView tvCurrentlyWatchingTitle;
    TextView tvEpisodeNumber;
    Button btnUpEpNumber;
    Button btnComplete;
      Shows show;








    public CurrentlyWatchingCellHolder(@NonNull View itemView,final ButtonClickListener showCompletedBtn) {
        super(itemView);
        ivCurrentlyWatching = itemView.findViewById(R.id.ivCurrentlyWatching);
        tvCurrentlyWatchingTitle = itemView.findViewById(R.id.tvCurrentlyWatchingTitle);
        btnUpEpNumber = itemView.findViewById(R.id.btnChangeEpNumber);
        btnUpEpNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show.UpEpisodeNumber();
                tvEpisodeNumber.setText("Ep: "+String.valueOf(show.getEpisodeNumber()));
                showCompletedBtn.upEpNumber(show);
            }
        });
        show=new Shows();

        btnComplete = itemView.findViewById(R.id.btnComplete);
        tvEpisodeNumber = itemView.findViewById(R.id.tvCurrentlyWatchingEpisodeNumber);
       btnComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCompletedBtn.CompleteButtonClicked(show);
            }
        });







        }


public void setShow(Shows show) {
    Picasso.get().load(show.getImageLink()).into(ivCurrentlyWatching);
    tvCurrentlyWatchingTitle.setText(show.getTitle());
    tvEpisodeNumber.setText("Ep: "+String.valueOf(show.getEpisodeNumber()));
    this.show.setImageLink(show.getImageLink());
    this.show.setTitle(show.getTitle());
    this.show.setEpisodeNumber(show.getEpisodeNumber());



}







}
