package barbarabilonic.ferit.episodetracker;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChooseShow extends Fragment {

    private RecyclerAdapterChooseShow recyclerAdapter;
    private RecyclerView recyclerView;
    private Call<ChooseTvShow> apiCall;
    private Button btnSearch;
    private EditText etSearchShow;
   private ButtonClickListener chooseShowbtn;
   private Button btnMostPopular;



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.chooseShowbtn = (ButtonClickListener) context;





    }


    @Override
    public void onDetach() {
        super.onDetach();
        chooseShowbtn = null;
    }
    public void setData(ChooseTvShow data){
        recyclerAdapter.addData(data);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.choose_show_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView=view.findViewById(R.id.ChooseShowRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
       recyclerAdapter=new RecyclerAdapterChooseShow(chooseShowbtn);
        recyclerView.setAdapter(recyclerAdapter);
        btnSearch=view.findViewById(R.id.btnSearch);
        btnMostPopular=view.findViewById(R.id.btnMostPopular);
        etSearchShow=view.findViewById(R.id.etSearchShow);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etSearchShow.getText().toString()!=""){
                    String text=etSearchShow.getText().toString();
                    text.replace(" ","-");
                    text.replace("\n","");
                    setUpApiCallSearch(text);
                    btnMostPopular.setText("Back To Most Popular");



                }


            }
        });
        btnMostPopular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpApiCallMostPopular();
                btnMostPopular.setText("Most Popular");
            }
        });
        setUpApiCallMostPopular();



    }




    public void setUpApiCallSearch(String condition){
        apiCall=NetworkUtils.getApiInterface().getItems(condition);
        apiCall.enqueue(
                new Callback<ChooseTvShow>() {
                    @Override
                    public void onResponse(Call<ChooseTvShow> call, Response<ChooseTvShow> response) {
                        if(response.isSuccessful() && response.body() != null){
                            recyclerAdapter.addData(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<ChooseTvShow> call, Throwable t) {
                        Toast.makeText(getContext(),"Error",Toast.LENGTH_LONG).show();
                    }


                });
    }
    private void setUpApiCallMostPopular(){
        apiCall=NetworkUtilsMostPopular.getApiInterface().getItems();
        apiCall.enqueue(
                new Callback<ChooseTvShow>() {
                    @Override
                    public void onResponse(Call<ChooseTvShow> call, Response<ChooseTvShow> response) {
                        if(response.isSuccessful() && response.body() != null){
                            recyclerAdapter.addData(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<ChooseTvShow> call, Throwable t) {
                        Toast.makeText(getContext(),"Error",Toast.LENGTH_LONG).show();
                    }


                });
    }
}
