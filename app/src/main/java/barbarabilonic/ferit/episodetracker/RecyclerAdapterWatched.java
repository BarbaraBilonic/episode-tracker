package barbarabilonic.ferit.episodetracker;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapterWatched extends RecyclerView.Adapter<WatchedCellHolder> {
   private List<Shows> shows=new ArrayList<>();

    @NonNull
    @Override
    public WatchedCellHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View cellView= LayoutInflater.from(parent.getContext()).inflate(R.layout.watched_shows_cell,parent,false);
        return new WatchedCellHolder(cellView);
    }

    @Override
    public void onBindViewHolder(@NonNull WatchedCellHolder holder, int position) {
        holder.setImageWatched(shows.get(position).getImageLink());
        holder.setWatchedTitle(shows.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return shows.size();
    }

    public void addData(List<Shows> data){
        this.shows.clear();
        this.shows.addAll(data);
        notifyDataSetChanged();
    }
    public void addItem(Shows show){
        shows.add(show);
        notifyItemInserted(shows.size()-1);
    }
}
