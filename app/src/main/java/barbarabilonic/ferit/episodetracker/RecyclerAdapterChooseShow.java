package barbarabilonic.ferit.episodetracker;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapterChooseShow extends RecyclerView.Adapter<ChooseShowCellHolder> {
    private List<TvShow> shows = new ArrayList<>();
    private ButtonClickListener chooseShowbtn;
    public RecyclerAdapterChooseShow(ButtonClickListener csb) {
        chooseShowbtn = csb;
    }

    @NonNull
    @Override
    public ChooseShowCellHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View cellView = LayoutInflater.from(parent.getContext()).inflate(R.layout.choose_show_cell, parent, false);
        return new ChooseShowCellHolder(cellView,chooseShowbtn);
    }

    @Override
    public void onBindViewHolder(@NonNull ChooseShowCellHolder holder, int position) {
        holder.setImageChooseShow(shows.get(position).getImageThumbnailPath());
        holder.setIvChooseShowTitle(shows.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return shows.size();
    }

    public void addData(ChooseTvShow data) {
        this.shows.clear();
        this.shows.addAll(data.getTvShows());
        notifyDataSetChanged();
    }


}

