package barbarabilonic.ferit.episodetracker;

import retrofit2.Call;
import retrofit2.http.GET;


public interface APIInterfaceMostPopular {
    @GET("/api/most-popular")
    Call<ChooseTvShow> getItems();
}
