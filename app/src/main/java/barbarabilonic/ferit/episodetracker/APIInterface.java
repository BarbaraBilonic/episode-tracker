package barbarabilonic.ferit.episodetracker;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIInterface {
    @GET("/api/search")
    Call<ChooseTvShow> getItems(@Query("q") String condition);
}
