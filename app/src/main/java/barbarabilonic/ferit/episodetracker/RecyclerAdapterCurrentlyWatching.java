package barbarabilonic.ferit.episodetracker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapterCurrentlyWatching extends RecyclerView.Adapter<CurrentlyWatchingCellHolder> {
    private List<Shows> shows=new ArrayList<>();

   private  ButtonClickListener showCompletedBtn;

    public RecyclerAdapterCurrentlyWatching(ButtonClickListener scb) {
        showCompletedBtn = scb;


    }


    @NonNull
    @Override
    public CurrentlyWatchingCellHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View cellView= LayoutInflater.from(parent.getContext()).inflate(R.layout.currently_watching_cell,parent,false);
        return new CurrentlyWatchingCellHolder(cellView,showCompletedBtn);
    }

    @Override
    public void onBindViewHolder(@NonNull final CurrentlyWatchingCellHolder holder, final int position) {
            holder.setShow(shows.get(position));






    }




    @Override
    public int getItemCount() {
        return shows.size();
    }

    public void addData(List<Shows> data){
        this.shows.clear();
        this.shows.addAll(data);
        notifyDataSetChanged();
    }

    public void addItem(Shows show){
        shows.add(show);
        notifyItemInserted(shows.size()-1);
    }

    public void removeItem(Shows show){
        if(shows.size()==1){
            shows.remove(0);
            notifyItemRemoved(0);
            notifyItemRangeChanged(0,getItemCount());
        }
        else {

            for (int i = 0; i < shows.size(); i++) {
                if (shows.get(i).getImageLink() == show.getImageLink()) {
                    shows.remove(i);
                    notifyItemRemoved(i);
                    notifyItemRangeChanged(i,getItemCount());


                }
            }
        }






    }





}
