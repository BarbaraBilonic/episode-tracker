package barbarabilonic.ferit.episodetracker;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Watched extends Fragment {
    List<Shows> shows;
    private RecyclerAdapterWatched recyclerAdapter;
    private RecyclerView recyclerView;
    public static final String SHARED_PREF_FILE="shared_pref_w";
    public static final String KEY="Watched";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(SHARED_PREF_FILE, Context.MODE_PRIVATE);
        Gson gson=new Gson();
        String json=sharedPreferences.getString(KEY,null);
        Type type=new TypeToken<ArrayList<Shows>>() {}.getType();
        shows=gson.fromJson(json,type);
        if(shows==null){
            shows=new ArrayList<>();
        }




    }






    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.watched_fragment, container, false);
        recyclerView= view.findViewById(R.id.WatchedRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerAdapter=new RecyclerAdapterWatched();
        recyclerView.setAdapter(recyclerAdapter);
        if(shows.size()>0){
            recyclerAdapter.addData(shows);
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

     ;
    }

 public void addItem(Shows show){
     Shows sh=new Shows();
     sh.setImageLink(show.getImageLink());
     sh.setTitle(show.getTitle());
     shows.add(sh);
     SharedPreferences sharedPreferences = getContext().getSharedPreferences(SHARED_PREF_FILE, Context.MODE_PRIVATE);
     SharedPreferences.Editor editor = sharedPreferences.edit();
     Gson gson=new Gson();
     String json=gson.toJson(shows);
     editor.clear();
     editor.putString(KEY,json);
     editor.apply();
        recyclerAdapter.addItem(show);
 }
}
