
package barbarabilonic.ferit.episodetracker;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class TvShow {

    @SerializedName("country")
    private String mCountry;
    @SerializedName("end_date")
    private String mEndDate;
    @SerializedName("id")
    private Long mId;
    @SerializedName("image_thumbnail_path")
    private String mImageThumbnailPath;
    @SerializedName("name")
    private String mName;
    @SerializedName("network")
    private String mNetwork;
    @SerializedName("permalink")
    private String mPermalink;
    @SerializedName("start_date")
    private String mStartDate;
    @SerializedName("status")
    private String mStatus;

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getEndDate() {
        return mEndDate;
    }

    public void setEndDate(String endDate) {
        mEndDate = endDate;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getImageThumbnailPath() {
        return mImageThumbnailPath;
    }

    public void setImageThumbnailPath(String imageThumbnailPath) {
        mImageThumbnailPath = imageThumbnailPath;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getNetwork() {
        return mNetwork;
    }

    public void setNetwork(String network) {
        mNetwork = network;
    }

    public String getPermalink() {
        return mPermalink;
    }

    public void setPermalink(String permalink) {
        mPermalink = permalink;
    }

    public String getStartDate() {
        return mStartDate;
    }

    public void setStartDate(String startDate) {
        mStartDate = startDate;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

}
