package barbarabilonic.ferit.episodetracker;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

public class WatchedCellHolder extends RecyclerView.ViewHolder {

    ImageView ivWatched;
    TextView tvWatchedTitle;


    public WatchedCellHolder(@NonNull View itemView) {
        super(itemView);
        ivWatched=itemView.findViewById(R.id.ivWatched);
        tvWatchedTitle=itemView.findViewById(R.id.tvwatchedTitle);

    }

    public void setImageWatched(String source){
        Picasso.get().load(source).into(ivWatched);
    }
    public void setWatchedTitle(String title){
        tvWatchedTitle.setText(title);
    }


}
