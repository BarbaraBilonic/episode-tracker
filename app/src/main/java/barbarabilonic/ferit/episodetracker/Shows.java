package barbarabilonic.ferit.episodetracker;

public class Shows {
    private String title;
    private String imageLink;
    private int episodeNumber;

    public String getImageLink(){
        return imageLink;
    }
    public String getTitle(){return  title;}


    public int getEpisodeNumber(){return episodeNumber;}

    public void setImageLink(String link){
        imageLink=link;
    }
    public void setTitle(String title){
        this.title=title;
    }
    public void setEpisodeNumber(int epNumber){
        episodeNumber=epNumber;
    }
    public void UpEpisodeNumber(){
        episodeNumber++;
    }

}
