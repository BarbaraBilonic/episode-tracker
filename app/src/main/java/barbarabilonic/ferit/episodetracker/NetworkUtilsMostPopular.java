package barbarabilonic.ferit.episodetracker;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkUtilsMostPopular {
    private static final String BASE_API = "https://www.episodate.com";
    private static APIInterfaceMostPopular apiInterface;


    public static APIInterfaceMostPopular getApiInterface() {

        if (apiInterface == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_API)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            apiInterface = retrofit.create(APIInterfaceMostPopular.class);
        }
        return apiInterface;


    }
}
