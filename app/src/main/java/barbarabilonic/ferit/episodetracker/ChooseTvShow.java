
package barbarabilonic.ferit.episodetracker;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class ChooseTvShow {

    @SerializedName("page")
    private Long mPage;
    @SerializedName("pages")
    private Long mPages;
    @SerializedName("total")
    private String mTotal;
    @SerializedName("tv_shows")
    private List<TvShow> mTvShows;

    public Long getPage() {
        return mPage;
    }

    public void setPage(Long page) {
        mPage = page;
    }

    public Long getPages() {
        return mPages;
    }

    public void setPages(Long pages) {
        mPages = pages;
    }

    public String getTotal() {
        return mTotal;
    }

    public void setTotal(String total) {
        mTotal = total;
    }

    public List<TvShow> getTvShows() {
        return mTvShows;
    }

    public void setTvShows(List<TvShow> tvShows) {
        mTvShows = tvShows;
    }

}
