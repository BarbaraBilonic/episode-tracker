package barbarabilonic.ferit.episodetracker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity  implements  ButtonClickListener{

     ViewPager mViewPager;
     TabLayout mTabLayout;
     CurrentlyWatching currentlyWatchingFragment;
    Watched watchedFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mViewPager = findViewById(R.id.viewPager);
        mViewPager.setOffscreenPageLimit(3);
        currentlyWatchingFragment= new CurrentlyWatching();

        watchedFragment= new Watched();
        ViewPagerAdapter viewPagerAdapter=new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.AddFragment(currentlyWatchingFragment);

        viewPagerAdapter.AddFragment(watchedFragment);
        viewPagerAdapter.AddFragment(new ChooseShow());
        mViewPager.setAdapter(viewPagerAdapter);
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();

    }










    @Override
    public void CompleteButtonClicked(Shows show) {
        currentlyWatchingFragment.removeItem(show);
        mViewPager.setCurrentItem(1);
        watchedFragment.addItem(show);
    }

    @Override
    public void ChooseShowButtonClicked(Shows show) {
        mViewPager.setCurrentItem(0);
        currentlyWatchingFragment.addItem(show);
    }

    @Override
    public void upEpNumber(Shows show) {
        currentlyWatchingFragment.upEpisodeNumber(show);
    }
}
