package barbarabilonic.ferit.episodetracker;

import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.Locale;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<Fragment> listOfFragments=new ArrayList<>();




    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int position) {

        return listOfFragments.get(position);

    }

    @Override
    public int getCount() {
        return listOfFragments.size();
    }
    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if(position==0) return "Currently Watching";
        if(position==1) return "Watched";
        return "Find New Show";
    }
    void AddFragment(Fragment newFragment){
        listOfFragments.add(newFragment);
    }


}
