package barbarabilonic.ferit.episodetracker;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

public class ChooseShowCellHolder extends  RecyclerView.ViewHolder {
    ImageView ivChooseShow;
    TextView tvChooseShow;
    Shows show;
    Button btnChooseShow;





    public ChooseShowCellHolder(@NonNull View itemView, final ButtonClickListener chooseShowbtn) {
        super(itemView);
        ivChooseShow = itemView.findViewById(R.id.ivChooseShow);
        tvChooseShow = itemView.findViewById(R.id.tvChooseShow);
        show = new Shows();


        btnChooseShow = itemView.findViewById(R.id.btnChooseShow);
        btnChooseShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               chooseShowbtn.ChooseShowButtonClicked(show);

            }
        });

    }



    public void setImageChooseShow(String source){
        Picasso.get().load(source).into(ivChooseShow);
        show.setImageLink(source);



    }
    public void setIvChooseShowTitle(String title){
        tvChooseShow.setText(title);
        show.setTitle(title);
    }




}
